package kr;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import kr.sskm.entity.Member;
import kr.sskm.entity.Team;

public class Main {
	//1
	//2
	//4
	//5
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 20201112 추가
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("hello");
		
		// EntityManager는 영속성 컨텍스트이다.
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		try {
			Team team = new Team();
			team.setName("TeamA");
			em.persist(team);	// 1차 캐시에 저장
			
			Member member = new Member();
			member.setName("member1");
			
			// 연관관계 주인에 값 설정
			member.setTeam(team);
			// 객체를 저장한 상태(영속)
			em.persist(member); // 1차 캐시에 저장
			
			// 역방향(주인이 아닌 방향)만 연관관계 설정
			team.getMembers().add(member);

/************ JPQL 샘플 fetch join 페이징 START ************

 test1 branch push
 test1 branch push2

 test1 branch push3


 20201112 추가
 ************ JPQL 샘플 fetch join 페이징 END *************************/	
			String jpql = "select m From Member m join fetch m.team where m.name like '%TEST%'";
			List<Member> result = em.createQuery(jpql, Member.class)
					.setFirstResult(10)
					.setMaxResults(20)
					.getResultList();
			System.out.println("JPQL result size : " + result.size());
			
			em.flush(); // 실제 DB에 저장(영속성 컨텍스트를 비우지 않음)
			em.clear();	// 영속성 컨텍스트 안에서 1차 캐시 삭제
			

/************ 영속성 컨텍스트 변경감지 및 Lazy Loading 확인용 START ************			
			Member findMember = em.find(Member.class, member.getId());
			
//			em.close(); // LazyInitializationException 발생 테스트 - 영속성 컨텍스트 close 이후 Lazy Loading 객체 Team에 접근 하려고 하면 오류!!
			Team findTeam = findMember.getTeam();
			System.out.println("findTeam : " + findTeam);
			findTeam.getName();
			
			// em.detach(findMember);	// 영속성 컨텍스트에서 더이상 관리하지 않겠다.(준영속)
			em.clear(); // 영속성 컨텍스트 안에서 1차캐시 삭제
			
			findMember.setName("member1-수정33");	// 값만 변경하고 em.update() 같은 액션을 하지 않고 아래 tx.commit()만으로도 DB 변경 - 영속성 컨텍스트 변경감지 때문!!
************ 영속성 컨텍스트 변경감지 및 Lazy Loading 확인용 END *************************/			
			tx.commit();
			
		}catch(Exception e) {
			e.printStackTrace();
			tx.rollback();
		}finally {
			em.close();
		}
		
		emf.close();
	}

}
